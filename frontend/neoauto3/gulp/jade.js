
var locals = {},
	time = new Date().getTime();

locals.baseUrl = 'http://local.neoauto3.com:3000/';
//locals.baseUrl = 'http://192.168.1.20:3000/';

locals.staticUrl = locals.baseUrl;
locals.elementUrl = locals.baseUrl;
locals.version = time;

//console.log(locals)

/*
* Exports locals
*/
module.exports = locals;
